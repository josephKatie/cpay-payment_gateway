package com.demo.cpay.base.enums;

public enum ChannelType {
    ALI, WECHAT, UNION;
}
